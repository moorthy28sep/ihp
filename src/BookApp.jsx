import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';


import CustomizedInputs from './components/TextField';
import RadioButtonsGroup from './components/RadioButtonsGroup';
import ContainButtons from './components/ContainedButtons';
import MobiCalc from './components/MobiCalc';


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

import * as mobiscroll from './mobiscroll';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { jsonDataAction } from './reducers/slotReducer';
import { labelDataAction } from './reducers/labelReducer';
import { markedKneeAction } from './reducers/markedKneeReducer';
import { labelsKneeAction } from './reducers/labelsKneeReducer';
import { markedVasectomyAction } from './reducers/markedVasectomyReducer';
import { labelsVasectomyAction } from './reducers/labelsVasectomyReducer';
mobiscroll.settings = {
    theme: 'ios'
};

function Transition(props) {
    return <Slide direction="up" {...props} />;
}


class BookApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Procedure: [
                { p: "Inguinal hernia" },
                { p: "Mastectomy for breast cancer", },
                { p: "Facelift sugery" },
                { p: "Breast implant" },
                { p: "Breast reconstruction after mastectomy" },
                { p: "Liposuction" },
                { p: "LASIK-laser eye surgery" },
                { p: "Tonsillectomy" },
                { p: "Total hip replacement" },
                { p: "Total knee replacement" },
                { p: "Vasectomy" }
            ],
            marked: [],
            labels: [],
            disabled: "disabled",
            checked: "checked",
            hidden: "hidden",
            cal1: "cal1",
            cal3: "cal3",
            cal6: "cal6",
            page1: "",
            page2: "hidden",
            pageTitle: "Book Appointment",
            costing: "",
            open: false,
            cost1: "cost1",
            cost3: "cost3",
            cost6: "cost6"
        };

    }

    /*componentWillMount() {
         var nodes = document.querySelectorAll("input#custom-css-outlined-input[type=text]");
        for (var i=0; i<nodes.length; i++) {
            if (nodes[i].value === "" || !/[0-9.]+/.test(nodes[i].value))
                return this.setState({disabled : "disabled"});
        } 
        
    }*/
    componentDidMount() {
        this.props.jsonKey();
        this.props.labelKey();
        this.props.markedKneeKey();
        this.props.labelsKneeKey();
        this.props.markedVasectomyKey();
        this.props.labelsVasectomyKey();
    }

    BackBtn = (event) => {
        this.setState({
            page1: "",
            page2: "hidden"
        });
    }
    handleClickOpen = () => {
        this.setState({ open: true });
        var proName = document.getElementById('procedureVal').innerHTML;

        var CostingDiv = document.getElementsByClassName("CostingDiv")[0].value;

        document.getElementById('confirmMsg').innerHTML = "Appointment for " + proName + " on " + CostingDiv + " is fixed.";

    };

    handleClose = () => {
        this.setState({
            open: false,
            page2: "hidden",
            page1: ""
        });
    };

    CheckCost = (event) => {

        this.setState({
            page1: "hidden",
            page2: ""
        });

        var radios = document.getElementsByName('months');

        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                // do whatever you want with the checked radio
                if (radios[i].value === "With-in 1 month ") {
                    this.setState({
                        cal1: "",
                        cal3: "cal3",
                        cal6: "cal6",
                        cost1: "",
                        cost3: "cost3",
                        cost6: "cost6"
                    });
                }
                else if (radios[i].value === "With-in 3 months ") {
                    this.setState({
                        cal1: "cal1",
                        cal3: "",
                        cal6: "cal6",
                        cost1: "cost1",
                        cost3: "",
                        cost6: "cost6"
                    });

                }
                else if (radios[i].value === "With-in 6 months ") {
                    this.setState({
                        cal1: "cal1",
                        cal3: "cal3",
                        cal6: "",
                        cost1: "cost1",
                        cost3: "cost3",
                        cost6: ""
                    });
                }

                // only one radio can be logically checked, don't check the rest
                //break;
            }
        }

        var dropdownVal = document.getElementById("mySelect").value;
        document.getElementById("procedureVal").innerHTML = dropdownVal;

    }

    producerFn = (event) => {
        var selectedVal = document.getElementById("mySelect").value;
        document.getElementById("procedureVal").innerHTML = selectedVal;

        if (selectedVal === "") {
            this.setState({
                marked: []
            });
        }
        else if (selectedVal === "Inguinal hernia") {
            this.setState({

                marked: this.props.jsonValue.myJson,
                labels: this.props.labelValue.labelJson
            });
        }
        else if (selectedVal === "Total knee replacement") {

            this.setState({
                marked: this.props.markedKneeJsonValue.markedKneeJson,
                labels: this.props.labelsKneeJsonValue.labelsKneeJson
            });
        }
        else if (selectedVal === "Vasectomy") {

            this.setState({
                marked: this.props.markedVasectomyJsonValue.markedVasectomyJson,
                labels: this.props.labelsVasectomyJsonValue.labelsVasectomyJson
            });
        }
    }

    monthRdo = (event) => {
        var textField = document.getElementsByClassName("MuiInputBase-input-262");
        for (var i = 0; i < textField.length; i++) {
            if (textField[i].value !== "") {
                if (document.getElementById("mySelect").value !== "") {
                    this.setState({
                        disabled: ""
                    });
                }

            }
        }
    }

    /* oneCal = (event) => {
             var myselect1 = document.getElementsByClassName("mbsc-comp")[1].value;
            alert(myselect1);
     }
     threeCal = (event) => {
         var myselect3 = document.getElementsByClassName("mbsc-comp")[6].value;
        alert(myselect3);
 }
 sixCal = (event) => {
     var myselect11 = document.getElementsByClassName("mbsc-comp")[11].value;
     alert(myselect11);
 }
 */

    onSetDate = (ev, inst) => {
        this.setState({
            selectedDate: inst.getVal()
        });
    }


    render() {
        return (
            <div className="container">
                <h1 className="pageTitle">{this.state.pageTitle}</h1>
                <form name="booking" className={this.state.page1}>
                    <Grid container spacing={24}>
                        <Grid item xs={12}>
                            <CustomizedInputs label={'MRN*'} txtClassName={'small'} />
                        </Grid>
                        <Grid item xs={6}>
                            <CustomizedInputs label={'First Name*'} txtClassName={'normal'} />
                        </Grid>
                        <Grid item xs={6}>
                            <CustomizedInputs label={'Last Name'} txtClassName={'normal'} />
                        </Grid>
                        <Grid item xs={6}>
                            <CustomizedInputs label={'Contact Number*'} txtClassName={'normal'} />
                        </Grid>
                        <Grid item xs={6}>
                            <CustomizedInputs label={'Address'} txtClassName={'normal'} />
                        </Grid>
                        <Grid item xs={6}>
                            <CustomizedInputs label={'Sex*'} txtClassName={'small'} />
                        </Grid>
                        <Grid item xs={6}>
                            <CustomizedInputs label={'Date of Birth'} type={'date'} txtClassName={'small'} />
                        </Grid>

                        <Grid item xs={12} sm={6} >
                            <label className="labelColor">Procedure Name</label>
                            <select name="Procedure" required id="mySelect" onChange={this.producerFn}>
                                <option value="">None *</option>
                                {this.state.Procedure.length !== 0 && this.state.Procedure.map((item, index) => {
                                    return (
                                        <option value={item.p} key={index}>{item.p}</option>
                                    )
                                }
                                )
                                }
                            </select>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <label className="labelColor">Doctor's Name</label>
                            <select name="doctorname" required id="mySelect1">
                                <option value="">Please select</option>
                                <option value="Lawrence">Dr. Lawrence</option>
                                <option value="Joseph">Dr. Joseph</option>
                                <option value="Robert">Dr. Robert</option>
                                <option value="Edward">Dr. Edward</option>
                                <option value="Peter">Dr. Peter</option>
                                <option value="Oma">Dr. Omar</option>
                                <option value="Ranna">Dr. Ranna</option>
                            </select>

                        </Grid>
                    </Grid>
                    <br />
                    <hr />
                    <Grid container spacing={24}>
                        <Grid item xs={4}>
                            <RadioButtonsGroup labels="With-in 1 month " onClick={this.monthRdo} value={'With-in 1 month '} />
                        </Grid>
                        <Grid item xs={4}>
                            <RadioButtonsGroup labels="With-in 3 months " onClick={this.monthRdo} value={'With-in 3 months '} />
                        </Grid>
                        <Grid item xs={4}>
                            <RadioButtonsGroup labels="With-in 6 months " onClick={this.monthRdo} value={'With-in 6 months '} />
                        </Grid>
                    </Grid>
                    <hr />
                    <Grid container spacing={24}>
                        <Grid container justify="center">
                            <ContainButtons label={'Check Procedure Cost'} onClick={this.CheckCost} disabled={this.state.disabled}></ContainButtons>


                        </Grid>
                    </Grid>
                </form>
                <div className={this.state.page2}>
                    <div className="btnAlignment"><ContainButtons label={'Back'} onClick={this.BackBtn} disabled={this.state.disabled}></ContainButtons></div>
                    <h3 id="procedureVal">Procedure</h3>
                    <p>Please select an available date for the chosen procedure.</p>
                    <p><span className="legend-mark"></span> - Denotes the booked slots.</p>
                    <div className={this.state.cal1}>
                        <mobiscroll.Form inputStyle="box" labelStyle="inline">
                            <mobiscroll.FormGroup>
                                <div className="mbsc-row mbsc-form-grid">

                                    <MobiCalc display={'inline'} value={this.state.selectedDate} onSet={this.onSetDate} marked={this.state.marked} months={'1'} />

                                </div>
                            </mobiscroll.FormGroup>
                        </mobiscroll.Form>
                    </div>
                    <div className={this.state.cal3}>
                        <mobiscroll.Form inputStyle="box" labelStyle="inline">
                            <mobiscroll.FormGroup>

                                <MobiCalc display={'inline'} value={this.state.selectedDate} onSet={this.onSetDate} marked={this.state.marked} months={'3'} />

                            </mobiscroll.FormGroup>
                        </mobiscroll.Form>
                    </div>
                    <div className={this.state.cal6}>
                        <mobiscroll.Form inputStyle="box" labelStyle="inline">
                            <mobiscroll.FormGroup>

                                <MobiCalc display={'inline'} value={this.state.selectedDate} onSet={this.onSetDate} marked={this.state.marked} months={'6'} />

                            </mobiscroll.FormGroup>
                        </mobiscroll.Form>
                    </div>
                    <div className={this.state.costing}>
                        <h3>Costing</h3>



                        <mobiscroll.Form inputStyle="box" labelStyle="inline">

                            <mobiscroll.FormGroup>
                                <div className=" ">

                                    <MobiCalc
                                        value={this.state.selectedDate}
                                        display={'inline'}
                                        labels={this.state.labels}
                                        months={1}
                                        className={'CostingDiv'}
                                    />
                                </div>
                            </mobiscroll.FormGroup>

                        </mobiscroll.Form>


                        <br className="clear" />



                        <Grid container spacing={24}>
                            <Grid container justify="center">
                                <div>
                                    <ContainButtons label={'Book Appointment'} onClick={this.handleClickOpen}>
                                    </ContainButtons>
                                    <Dialog
                                        open={this.state.open}
                                        TransitionComponent={Transition}
                                        keepMounted
                                        onClose={this.handleClose}
                                        aria-labelledby="alert-dialog-slide-title"
                                        aria-describedby="alert-dialog-slide-description"
                                    >
                                        <DialogTitle id="alert-dialog-slide-title">

                                            <div id="confirmMsg"></div>

                                        </DialogTitle>
                                        <DialogContent>
                                            <DialogContentText id="alert-dialog-slide-description"></DialogContentText>
                                        </DialogContent>
                                        <DialogActions>

                                            <ContainButtons onClick={this.handleClose} label={'Ok'} >

                                            </ContainButtons>
                                        </DialogActions>
                                    </Dialog>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                </div>





            </div>
        );
    }
}

const mapStateToprops = (state) => {
    return {
        labelValue: state.labelview,
        jsonValue: state.slotview,
        markedKneeJsonValue: state.markedKneeView,
        labelsKneeJsonValue: state.labelsKneeView,
        markedVasectomyJsonValue: state.markedVasectomyView,
        labelsVasectomyJsonValue: state.labelsVasectomyView,

    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        jsonKey: jsonDataAction,
        labelKey: labelDataAction,
        markedKneeKey: markedKneeAction,
        labelsKneeKey: labelsKneeAction,
        markedVasectomyKey: markedVasectomyAction,
        labelsVasectomyKey: labelsVasectomyAction
    }, dispatch);
}

export default connect(mapStateToprops, mapDispatchToProps)(BookApp);