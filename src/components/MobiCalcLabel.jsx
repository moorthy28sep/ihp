import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import * as mobiscroll from '../mobiscroll';
import '../mobiscroll.react.min.css';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

function MobiCalcLabel(props) {
  const { labels, months, display, onClick } = props;
  return (
    <div>
                                <label>
                                    
                                    <mobiscroll.Calendar
                                        display={display}
                                        type="hidden"
                                        labels={labels}
                                        months={months}
                                        yearChange={false}
                                        onClick={onClick}
                                        placeholder="Please Select..."
                                        className="CostingDiv"
                                    />
                                </label>
                            </div>
  );
}



export default withStyles(styles)(MobiCalcLabel);
