import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import ContainButtons from '../components/ContainedButtons';


function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class AlertDialogSlide extends React.Component {

  constructor(props) {
    super(props);              
    this.state = {
    open: false,
      page2 : ""
  };
}

  handleClickOpen = () => {
    this.setState({ open: true });
    var proName = document.getElementById('procedureVal').innerHTML;

    var CostingDiv = document.getElementsByClassName("CostingDiv")[0].value;

    document.getElementById('confirmMsg').innerHTML = "Appointment for " + proName + " on " + CostingDiv +" is fixed.";
    
  };

  handleClose = () => {
    this.setState({ 
      open: false, 
      page2 : "hidden" });
  };

  

  render() {
    return (
      <div>
       <ContainButtons label={'Book Appointment'} onClick={this.handleClickOpen}>
        </ContainButtons>
        <Dialog
          open={this.state.open}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
          
          <div id="confirmMsg"></div>
            
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description"></DialogContentText>
          </DialogContent>
          <DialogActions>
           
            <ContainButtons onClick={this.handleClose}  label={'Ok'} >
              
            </ContainButtons>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default AlertDialogSlide;
