import { put, takeLatest, all } from 'redux-saga/effects';


function* fetchslot() {
   
    const json =  yield fetch('http://localhost:3004/marked')
     .then(response => response.json()); 

     yield put({ type: "DATA_RECEIVED", data : { marked : json}

    //yield put({ type: "DATA_RECEIVED", data : { marked : [ { "Procedure": "Inguinal hernia", "color": "#f13f77", "d": "2019-05-05", "text": "$6985.01" }] }
    
    }            
    );
}

function* fetchlabel() {
   
     const json =  yield fetch('http://localhost:3004/labels')
      .then(response => response.json()); 
 
      yield put({ type: "LABELS_RECEIVED", data : { labels : json}
 
     //yield put({ type: "DATA_RECEIVED", data : { marked : [ { "Procedure": "Inguinal hernia", "color": "#f13f77", "d": "2019-05-05", "text": "$6985.01" }] }
     
     }            
     );
 }

 function* fetchKneeMarked() {
   
     const json =  yield fetch('http://localhost:3004/kneemarked')
      .then(response => response.json()); 
 
      yield put({ type: "MARKED_KNEE_RECEIVED", data : { marked : json}      
     
     }            
     );
 }
 function* fetchKneeLabels() {
   
     const json =  yield fetch('http://localhost:3004/kneelabels')
      .then(response => response.json()); 
 
      yield put({ type: "LABELS_KNEE_RECEIVED", data : { labels : json}      
     
     }            
     );
 }

 function* fetchVasectomyMarked() {
   
     const json =  yield fetch('http://localhost:3004/vasectomymarked')
      .then(response => response.json()); 
 
      yield put({ type: "MARKED_VASECTOMY_RECEIVED", data : { marked : json}     
     
     }            
     );
 }
 function* fetchVasectomyLabels() {
   
     const json =  yield fetch('http://localhost:3004/vasectomylabels')
      .then(response => response.json()); 
 
      yield put({ type: "LABELS_VASECTOMY_RECEIVED", data : { labels : json}
           
     }            
     );
 }

function* actionWatcher() {
     yield takeLatest('jsonDataType', fetchslot)
}

function* labelWatcher() {
     yield takeLatest('labelDataType', fetchlabel)
}

function* markedKneeWatcher() {
     yield takeLatest('markedKneeDataType', fetchKneeMarked)
}

function* labelsKneeWatcher() {
     yield takeLatest('labelsKneeDataType', fetchKneeLabels)
}

function* markedvasectomyWatcher() {
     yield takeLatest('markedVasectomyDataType', fetchVasectomyMarked)
}

function* labelsvasectomyWatcher() {
     yield takeLatest('markedKneeDataType', fetchVasectomyLabels)
}

export default function* rootSaga() {
   yield all([
        actionWatcher(),
        labelWatcher(),
        markedKneeWatcher(),
        labelsKneeWatcher(),
        markedvasectomyWatcher(),
        labelsvasectomyWatcher()

   ]);
}