import {combineReducers} from 'redux';
import slotReducer from './slotReducer';
import labelReducer from './labelReducer';
import markedKneeReducer from './markedKneeReducer';
import labelsKneeReducer from './labelsKneeReducer';
import markedVasectomyReducer from './markedVasectomyReducer';
import labelsVasectomyReducer from './labelsVasectomyReducer';

const allReducers = combineReducers({
    slotview : slotReducer,
    labelview : labelReducer,
    markedKneeView : markedKneeReducer,
    labelsKneeView : labelsKneeReducer,
    markedVasectomyView : markedVasectomyReducer,
    labelsVasectomyView : labelsVasectomyReducer
});

export default allReducers;