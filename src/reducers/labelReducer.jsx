const initialState = {
    labelJson: []
};

export const labelDataAction = () => {
    return {
        type: "labelDataType"
    }
}


export default(state = initialState, action) => {
    switch (action.type) {
        case "LABELS_RECEIVED": return labelDataFunction(state, action);
        default: return state;
    }
}

const labelDataFunction = (state, action) => {
    
    return {
        ...state,
        labelJson: action.data.labels
        
    }
}