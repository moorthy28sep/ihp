const initialState = {
    labelsVasectomyJson: []
};

export const labelsVasectomyAction = () => {
    return {
        type: "labelsVasectomyDataType"
    }
}


export default(state = initialState, action) => {
    switch (action.type) {
        case "LABELS_VASECTOMY_RECEIVED": return labelsVasectomyDataFunction(state, action);
        default: return state;
    }
}

const labelsVasectomyDataFunction = (state, action) => {
    
    return {
        ...state,
        labelsVasectomyJson: action.data.labels
        
    }
}