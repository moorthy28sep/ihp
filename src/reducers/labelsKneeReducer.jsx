const initialState = {
    labelsKneeJson: []
};

export const labelsKneeAction = () => {
    return {
        type: "labelsKneeDataType"
    }
}


export default(state = initialState, action) => {
    switch (action.type) {
        case "LABELS_KNEE_RECEIVED": return labelsKneeDataFunction(state, action);
        default: return state;
    }
}

const labelsKneeDataFunction = (state, action) => {
    
    return {
        ...state,
        labelsKneeJson: action.data.labels
        
    }
}