const initialState = {
    markedKneeJson: []
};

export const markedKneeAction = () => {
    return {
        type: "markedKneeDataType"
    }
}


export default(state = initialState, action) => {
    switch (action.type) {
        case "MARKED_KNEE_RECEIVED": return markedKneeDataFunction(state, action);
        default: return state;
    }
}

const markedKneeDataFunction = (state, action) => {
    
    return {
        ...state,
        markedKneeJson: action.data.marked
        
    }
}