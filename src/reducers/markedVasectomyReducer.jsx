const initialState = {
    markedVasectomyJson: []
};

export const markedVasectomyAction = () => {
    return {
        type: "markedVasectomyDataType"
    }
}


export default(state = initialState, action) => {
    switch (action.type) {
        case "MARKED_VASECTOMY_RECEIVED": return markedVasectomyDataFunction(state, action);
        default: return state;
    }
}

const markedVasectomyDataFunction = (state, action) => {
    
    return {
        ...state,
        markedVasectomyJson: action.data.marked
        
    }
}